#!/bin/bash

set -eu

debci_base_dir=$(readlink -f $(dirname $(readlink -f $0))/../..)
cd $debci_base_dir
. lib/environment.sh
. lib/functions.sh

while [ "$1" != '--' ]; do
  shift
done
shift

if [ -z "${WORKER_START_TIMESTAMP:-}" ]; then
  echo "E: WORKER_START_TIMESTAMP not defined"
  exit 1
fi

requested_packages="$@"
if [ -z "$requested_packages" ]; then
  requested_packages=$(cat $debci_config_dir/whitelist)
fi

shopt -s nullglob
for pkg in $requested_packages; do
  while true; do
    # wait for package history file to be more recent than the start time of
    # the current running worker
    history_file=$(echo $(status_dir_for_package $pkg)/history.json)
    if [ -f "$history_file" ]; then
      stamp=$(stat --format=%Y "$history_file")
      if [ $stamp -ge "${WORKER_START_TIMESTAMP}" ]; then
        break
      fi
    fi
    sleep ${debci_batch_poll_interval:-1}
  done
done

debci update
